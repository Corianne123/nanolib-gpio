#include "gpio.h"
#include <avr/io.h>


 void init_input_GPIO(t_nano_pin pin, t_input_mode mode) {
    volatile uint8_t * port ;
    volatile uint8_t * ddr ;
    
    if(pin >= D0 && pin <=D7) {
        port = &PORTD ;
        ddr = &DDRD;
    }
    if((pin >=D8) && (pin < A0) ) {
        port = &PORTB ;
        ddr = &DDRB;
        pin = pin - 8;
    }
    if(pin >= A0) {
        port = &PORTC ;
        ddr = &DDRC;
        pin = pin - 14;
    }


    *ddr &= ~_BV(pin); // configurer en entrée
    if(mode == PULLUP) {
        *port |= _BV(pin);
    } else {
        *port &= ~ _BV(pin);
    }
   
}

void init_output_GPIO(t_nano_pin pin) {
    volatile uint8_t * ddr;
    
    if(pin >= D0 && pin <=D7) {
        ddr = &DDRD;
    }
    if((pin >=D8) && (pin < A0)) {
        ddr = &DDRB;
         pin = pin - 8;
    }
    if(pin >= A0) {
        ddr = &DDRC;
         pin = pin - 14;
    }

   *ddr |= _BV(pin); // configurer en entrée
}

t_pin_state read_input_GPIO(t_nano_pin pin) {
    volatile uint8_t * pinx;
    if(pin >= D0 && pin <=D7) {
        pinx = &PIND;
    }
    if((pin >=D8) && (pin < A0)) {
        pinx = &PINB;
         pin = pin - 8;
    }
    if(pin >= A0) {
        pinx = &PINC;
         pin = pin - 14;
    }
   if (*pinx & (1 << pin)) return LOW;
   else return HIGH;
}

void write_output_GPIO(t_nano_pin pin, t_pin_state state) {

    volatile uint8_t * port;
    
    if(pin >= D0 && pin <=D7) {
        port =&PORTD ;
        
    }
    if((pin >=D8) && (pin < A0)) {
        port =&PORTB ;
         pin = pin - 8;
    }
    if(pin >= A0) {
        port =&PORTC ;
         pin = pin - 14;
    }

    if(state == LOW) {
        *port &= ~_BV(pin);
    } else {
        *port |=  _BV(pin);
    }
}
 
#include "gpio.h"


int main() {
  t_nano_pin button;
  t_nano_pin output1;
  t_nano_pin output2;

  // Assign a value to the variable
  button = A4; // A5
  output1 = D12;
  output2 = D8;

  init_input_GPIO(button,PULLUP);
  init_output_GPIO(output1);
  init_output_GPIO(output2);

  while (1) {
  
    if (!read_input_GPIO(button)) {
      write_output_GPIO (output2,HIGH);
      write_output_GPIO (output1,LOW);
    }else
    {
      write_output_GPIO (output1,HIGH);
      write_output_GPIO (output2,LOW);
    }
     }
    return 0;
}
